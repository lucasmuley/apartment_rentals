![picture](https://apartments.muley.es/logo_large.png)

## About Apartment Rentals

Apartment Rentals is an App that allows users to search apartments with Google Maps. 

There are three different roles:
- Admin: admin role have control of all the app. Can List, Create, Update, and Delete all the users and all the apartments created. Also, it can search apartments as a regular user.
- Realtor: realtor users can List, Create, Update and Delete their apartments. Also, they can search courses and see rented and no rented apartments.
- Clients: clients only can search apartments.

You can see a running example in the following site: [https://apartments.muley.es](https://apartments.muley.es)

## Technology

I have used Laravel for the backend and Vue.js for the frontend of the application. Frontend communicates with the backend through an API REST that has been programmed in the backed. This API REST use [Sanctum](https://laravel.com/docs/8.x/sanctum) to authentification. 

For the frontend I have used several packages: 

- [Vuetify framework](https://vuetifyjs.com/en/) as a base of design.
- [VueNoty](https://github.com/renoguyon/vuejs-noty) to display notifications.
- [VueGooglePlaces](https://www.npmjs.com/package/vue-google-places) to search places with the google api.
- [GmapVue](https://diegoazh.github.io/gmap-vue/#about-gmapvue) to manage Google Maps.
- [VueScrollTo](https://www.npmjs.com/package/vue-scrollto) to scroll page to one specific element.


## Installation

To install this app, you need two different servers: one for the backend and the frontend. You will see in the repository that you have two folders named backed and frontend for each part of the application.

### Frontend installation

You need to create a .env file like this:

```
VUE_APP_API_PATH=http://localhost
VUE_APP_PLACE_KEY=XXXXXXXXXXXXXXXXXXXX
VUE_APP_GOOGLE_MAPS_API_KEY=XXXXXXXXXXXXXXXXXXXXXXXXXXXX
```

As you can see, you have to set the API path and set two google API keys, one for the Google Place Search Field and another to use the Google Maps Javascript.

After that, you can install modules:

```
npm install
```

And, execute:

```
npm run serve
```

Or, publish:

```
npm run build
```

### Backend installation

You need to create a .env file. You can copy the .env.example. Modify the following lines depending on your requirements:

```
APP_NAME=Laravel
APP_ENV=local
APP_DEBUG=true
APP_URL=http://localhost

APP_VUE_URL=http://localhost:8080

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=laravel
DB_USERNAME=root
DB_PASSWORD=

MAIL_MAILER=smtp
MAIL_HOST=mailhog
MAIL_PORT=1025
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
MAIL_FROM_ADDRESS=null
MAIL_FROM_NAME="${APP_NAME}"

L5_SWAGGER_GENERATE_ALWAYS=true
```

You need to set a correct SMTP configuration to be able to reset your password. Also, you need to set the APP_VUE_URL because the email sent has an URL with the page to restart the password.

After doing that, you can install Laravel:

```
php composer install
```

After that, run the migrations to create all the tables in the database:

```
php artisan migrate
```

It is convenient to seed the database with the sample users and apartments.

```
php artisan db:seed
```

It will create an admin user. The credentials are:

Username: admin@example.com

Password: password

With this data, you are ready to test the frontend.


