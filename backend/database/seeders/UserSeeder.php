<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                    'name' => 'Admin User',
                    'email' => 'admin@example.com',
                    'password' => Hash::make('password'),
                    'role_id' => 1,
                    'created_at' => now()
                ],
                [
                    'name' => 'Realtor 1',
                    'email' => 'realtor1@example.com',
                    'password' => Hash::make('password'),
                    'role_id' => 2,
                    'created_at' => now()
                ],
                [
                    'name' => 'Realtor 2',
                    'email' => 'realtor2@example.com',
                    'password' => Hash::make('password'),
                    'role_id' => 2,
                    'created_at' => now()
                ],
                [
                    'name' => 'Realtor 3',
                    'email' => 'realtor3@example.com',
                    'password' => Hash::make('password'),
                    'role_id' => 2,
                    'created_at' => now()
                ],
                [
                    'name' => 'Client 1',
                    'email' => 'client1@example.com',
                    'password' => Hash::make('password'),
                    'role_id' => 3,
                    'created_at' => now()
                ],
                [
                    'name' => 'Client 2',
                    'email' => 'client2@example.com',
                    'password' => Hash::make('password'),
                    'role_id' => 3,
                    'created_at' => now()
                ],
                [
                    'name' => 'Client 3',
                    'email' => 'client3@example.com',
                    'password' => Hash::make('password'),
                    'role_id' => 3,
                    'created_at' => now()
                ],
            ]
        );
    }
}
