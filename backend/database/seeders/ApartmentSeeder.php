<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class ApartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('apartments')->insert(
            [
                array('id' => '1','name' => 'Apartment 3 rooms 2 baths','description' => 'This is a description','floor_area_size' => '100.00','price_per_month' => '700.00','n_rooms' => '3','realtor_id' => '2','latitude' => '41.65301040','longitude' => '-0.90607190','rented' => '0','address_type' => '1','address' => 'Av. de Madrid, Zaragoza, España', 'created_at' => now()),
                array('id' => '2','name' => 'Luxurious apartment','description' => 'This is an amazing apartment','floor_area_size' => '200.00','price_per_month' => '1900.00','n_rooms' => '5','realtor_id' => '3','latitude' => '41.64964060','longitude' => '-0.88335140','rented' => '0','address_type' => '1','address' => 'Paseo de la Independencia, Zaragoza, España', 'created_at' => now()),
                array('id' => '3','name' => 'Student apartment','description' => 'A bad apartment','floor_area_size' => '45.00','price_per_month' => '300.00','n_rooms' => '1','realtor_id' => '4','latitude' => '41.65498340','longitude' => '-0.88020120','rented' => '1','address_type' => '1','address' => 'Calle de Alfonso I, 50003 Zaragoza, España', 'created_at' => now()),
                array('id' => '4','name' => 'Family apartment','description' => 'This is the description.','floor_area_size' => '80.00','price_per_month' => '450.00','n_rooms' => '2','realtor_id' => '2','latitude' => '41.64779940','longitude' => '-0.90846210','rented' => '1','address_type' => '1','address' => 'Vía Univérsitas, Zaragoza, España', 'created_at' => now()),
                array('id' => '5','name' => 'New apartment','description' => 'New apartment next to railway station','floor_area_size' => '90.00','price_per_month' => '550.00','n_rooms' => '3','realtor_id' => '3','latitude' => '41.65627840','longitude' => '-0.91133480','rented' => '0','address_type' => '1','address' => 'Av. Navarra, Zaragoza, España', 'created_at' => now()),
                array('id' => '6','name' => 'Reformed flat','description' => 'This is a reformed flat','floor_area_size' => '68.00','price_per_month' => '400.00','n_rooms' => '2','realtor_id' => '4','latitude' => '41.65485480','longitude' => '-0.90855610','rented' => '1','address_type' => '1','address' => 'Calle Arias, 50010 Zaragoza, España', 'created_at' => now()),
                array('id' => '7','name' => 'Ancient flat','description' => 'Ancient flat but very economic','floor_area_size' => '55.00','price_per_month' => '150.00','n_rooms' => '2','realtor_id' => '3','latitude' => '41.65041210','longitude' => '-0.90484210','rented' => '1','address_type' => '1','address' => 'Calle Delicias, Zaragoza, España', 'created_at' => now()),
                array('id' => '8','name' => 'San José Apartment','description' => 'This is an amazing apartment that belongs to Rubén.','floor_area_size' => '90.00','price_per_month' => '430.00','n_rooms' => '4','realtor_id' => '3','latitude' => '41.64052160','longitude' => '-0.87376540','rented' => '0','address_type' => '1','address' => 'Av. Cesáreo Alierta, Zaragoza, España', 'created_at' => now())
            ]
        );
    }
}
