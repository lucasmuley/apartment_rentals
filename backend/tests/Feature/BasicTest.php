<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use Database\Seeders\UserSeeder;
use Database\Seeders\ApartmentSeeder;

class BasicTest extends TestCase
{
    use RefreshDatabase;
    protected $seed = true;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_basic()
    {

        //Main page
        $response = $this->get('/');
        $response->assertStatus(200);
    }

}
