<?php

use Illuminate\Support\Facades\Route;

use Illuminate\Http\Request;

use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\SanctumController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

 /**
  * Sanctum Routes
  */
Route::post('sanctum/token', [SanctumController::class, 'token'])->name('sanctum.token');
Route::post('api/register', [UserController::class, 'register'])->name('user.register'); 
Route::post('api/forgot', [UserController::class, 'forgot'])->name('user.forgot'); 
Route::post('api/restart_password', [UserController::class, 'restart_password'])->name('user.restart_password'); 
