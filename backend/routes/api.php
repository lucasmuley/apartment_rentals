<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\ApartmentController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\SanctumController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => ['auth:sanctum'],
], function ($router) {

    Route::post('apartment/search', [ApartmentController::class, 'search'])->name('apartment.search');
    Route::post('apartment/limits', [ApartmentController::class, 'limits'])->name('apartment.limits');
    Route::post('sanctum/close', [SanctumController::class, 'close'])->name('sanctum.close'); 

    // Realtor + Admin
    Route::group(
        [
            'middleware' => ['realtorRoutes']
        ],
        function () {

            Route::get('apartment', [ApartmentController::class, 'index'])->name('apartment.index');
            Route::post('apartment', [ApartmentController::class, 'store'])->name('apartment.store');  
            Route::put('apartment/{id}', [ApartmentController::class, 'update'])->name('apartment.update');   
            Route::delete('apartment/{id}', [ApartmentController::class, 'destroy'])->name('apartment.destroy');            

        });

    // Admin
    Route::group(
        [
            'middleware' => ['adminRoutes']
        ],
        function () {

            Route::get('user', [UserController::class, 'index'])->name('user.index');  
            Route::post('user', [UserController::class, 'store'])->name('user.store');  
            Route::put('user/{id}', [UserController::class, 'update'])->name('user.update');   
            Route::delete('user/{id}', [UserController::class, 'destroy'])->name('user.destroy');            

        });

});
