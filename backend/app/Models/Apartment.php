<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Apartment extends Model
{
    use HasFactory, SoftDeletes;

    public function Realtor()
    {
        return $this->belongsTo('App\Models\User', 'realtor_id', 'id');
    }

    public static function getById($apartment_id){
        return self::with('Realtor')->where('id',$apartment_id)->first();
    }

    public static function getAll(){
        $apartments = self::with('Realtor')->get();
        return $apartments;
    }

    public static function getAllAvailable(){
        $apartments = self::with('Realtor')->where('rented',0)->get();
        return $apartments;
    }

    public static function storeRecord($request){
        if($request->address_type == 1){
            if(!isset($request->address) || !isset($request->addressComponent) || $request->address == ''){
                return ['success' => false, 'message' => 'Invalid address'];
            }else{
                $latitude = $request->addressComponent['latitude'];
                $longitude = $request->addressComponent['longitude'];
                $address = $request->address;
            }
        }else{
            if(!isset($request->latitude) || !isset($request->longitude) || $request->latitude == '' || $request->longitude == ''){
                return ['success' => false, 'message' => 'Invalid address'];
            }else{
                if($request->latitude > 90 || $request->latitude < -90)
                    return ['success' => false, 'message' => 'Latitude must be between -90 and 90'];
                if($request->longitude > 180 || $request->longitude < -180)
                    return ['success' => false, 'message' => 'Longitude must be between -180 and 180'];
                $latitude = $request->latitude;
                $longitude = $request->longitude;
                $address = '';
            }
        }
        $apartment = new Apartment();
        $apartment->name = $request->name;
        $apartment->description = $request->description;
        $apartment->floor_area_size = $request->floor_area_size;
        $apartment->price_per_month = $request->price_per_month;
        $apartment->n_rooms = $request->n_rooms;
        $apartment->latitude = $latitude;
        $apartment->longitude = $longitude;
        $apartment->address = $address;
        $apartment->address_type = $request->address_type;
        $apartment->rented = $request->rented;
        if(Auth::user()->role_id == User::$REALTOR_ROLE){
            $apartment->realtor_id = Auth::id();   
        }elseif(Auth::user()->role_id == User::$ADMIN_ROLE){
            $apartment->realtor_id = $request->realtor_id;  
        }    
        $apartment->save();

        return ['success' => true, 'record' => $apartment];
    }

    public static function updateRecord($request, $apartment_id){

        $apartment = Apartment::find($apartment_id);
        if($apartment){
            if($request->address_type == 1){
                if(!isset($request->address) || !isset($request->addressComponent) || $request->address == ''){
                    return ['success' => false, 'message' => 'Invalid address'];
                }else{
                    if($request->address != $apartment->address){
                        $latitude = $request->addressComponent['latitude'];
                        $longitude = $request->addressComponent['longitude'];
                        $address = $request->address;
                    }else{
                        $latitude = $apartment->latitude;
                        $longitude = $apartment->longitude;
                        $address = $apartment->address;
                    }
                }
            }else{
                if(!isset($request->latitude) || !isset($request->longitude) || $request->latitude == '' || $request->longitude == ''){
                    return ['success' => false, 'message' => 'Invalid address'];
                }else{
                    if($request->latitude > 90 || $request->latitude < -90)
                        return ['success' => false, 'message' => 'Latitude must be between -90 and 90'];
                    if($request->longitude > 180 || $request->longitude < -180)
                        return ['success' => false, 'message' => 'Longitude must be between -180 and 180'];
                    $latitude = $request->latitude;
                    $longitude = $request->longitude;
                    $address = '';
                }
            }

            $apartment->name = $request->name;
            $apartment->description = $request->description;
            $apartment->floor_area_size = $request->floor_area_size;
            $apartment->price_per_month = $request->price_per_month;
            $apartment->n_rooms = $request->n_rooms;
            $apartment->latitude = $latitude;
            $apartment->longitude = $longitude;
            $apartment->address = $address;
            $apartment->address_type = $request->address_type;
            $apartment->rented = $request->rented;
            if(Auth::user()->role_id == User::$REALTOR_ROLE){
                $apartment->realtor_id = Auth::id();   
            }elseif(Auth::user()->role_id == User::$ADMIN_ROLE){
                $apartment->realtor_id = $request->realtor_id;  
            }           
            $apartment->save();

            return ['success' => true, 'record' => $apartment];
        }else{
            return ['success' => false, 'message' => 'Apartment not found'];
        }
    }
}
