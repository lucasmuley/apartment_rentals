<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;

use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    public static $ADMIN_ROLE = 1;
    public static $REALTOR_ROLE = 2;
    public static $CLIENT_ROLE = 3;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function storeRecord($request){

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->role_id = $request->role_id;
        $user->save();

        return ['success' => true, 'record' => $user];
    }

    public static function updateRecord($request){

        $user = User::find($request->id);
        $user->name = $request->name;
        if(isset($request->password) && $request->password != '')
            $user->password = Hash::make($request->password);
        $user->role_id = $request->role_id;
        $user->save();

        return ['success' => true, 'record' => $user];
    }

    public static function getAll(){
        $users = self::all();
        return $users;
    }

}
