<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;

use App\Models\User;

use App\Models\Apartment;
use App\Http\Requests\StoreApartmentRequest;

class ApartmentController extends Controller
{    
    /**
     *  @OA\Post(
     *    path="/apartment/limits",
     *    tags={"Apartment"},
     *    security={
     *      {"passport": {}},
     *    },
     *    summary="Limits to search apartments",
     *    description="Get the limits available to search apartments",
     *    @OA\Response(
     *      response=204,
     *      description="Successful operation, returned all",
     *      @OA\MediaType(
     *        mediaType="application/json",
     *      )
     *    ),
     *  )
     */
    public function limits(Request $request)
    {
        $query = DB::table('apartments')
            ->selectRaw('MAX(floor_area_size) AS max_floor_area_size, MIN(floor_area_size) AS min_floor_area_size, MAX(price_per_month) AS max_price_per_month, MIN(price_per_month) AS min_price_per_month, MAX(n_rooms) AS max_n_rooms, MIN(n_rooms) AS min_n_rooms');        
        if(Auth::user()->role_id == User::$CLIENT_ROLE)
            $query->where('rented',0);
        $limits = $query->get();
        return response()->json(['success' => true, 'limits' => $limits],200);
    }

    /**
     *  @OA\Post(
     *    path="/apartment/search",
     *    tags={"Apartment"},
     *    security={
     *      {"passport": {}},
     *    },
     *    summary="Search apartments",
     *    description="Search apartments",
     *    @OA\Parameter(
     *      name="floor_area_size",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *        type="numeric"
     *      )
     *    ),
     *    @OA\Parameter(
     *      name="price_per_month",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *        type="numeric"
     *      )
     *    ),
     *    @OA\Parameter(
     *      name="n_rooms",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *        type="integer"
     *      )
     *    ),
     *    @OA\Parameter(
     *      name="latitude",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *        type="float"
     *      )
     *    ),
     *    @OA\Parameter(
     *      name="longitude",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *        type="float"
     *      )
     *    ),
     *    @OA\Response(
     *      response=204,
     *      description="Successful operation, returned all",
     *      @OA\MediaType(
     *        mediaType="application/json",
     *      )
     *    ),
     *  )
     */
    public function search(Request $request)
    {
        $query = Apartment::query();
        if(isset($request->floor_area_size[0]))
            $query->where('floor_area_size','>=',$request->floor_area_size[0]);
        if(isset($request->floor_area_size[1]))
            $query->where('floor_area_size','<=',$request->floor_area_size[1]);
        if(isset($request->price_per_month[0]))
            $query->where('price_per_month','>=',$request->price_per_month[0]);
        if(isset($request->price_per_month[1]))
            $query->where('price_per_month','<=',$request->price_per_month[1]);
        if(isset($request->n_rooms[0]))
            $query->where('n_rooms','>=',$request->n_rooms[0]);
        if(isset($request->n_rooms[1]))
            $query->where('n_rooms','<=',$request->n_rooms[1]);
        if(isset($request->latitude['g']) && isset($request->latitude['g'])){
            if($request->latitude['g'] != $request->latitude['i']){
                $query->where('latitude','>=',$request->latitude['g']);
                $query->where('latitude','<=',$request->latitude['i']);
            }
        }
        if(isset($request->longitude['g']) && isset($request->longitude['i'])){
            if($request->longitude['g'] != $request->longitude['i']){
                $query->where('longitude','>=',$request->longitude['g']);
                $query->where('longitude','<=',$request->longitude['i']);
            }
        }

        if(Auth::user()->role_id == User::$CLIENT_ROLE){
            $query->where('rented',0);
            $total = Apartment::where('rented',0)->count();
        }else{
            $total = Apartment::count();
        }
        $apartments = $query->with('Realtor')->get();
        return response()->json(['success' => true, 'apartments' => $apartments, 'total' => $total, 'request' => $request->toArray()],200);
    }

    /**
     *  @OA\Get(
     *    path="/apartment",
     *    tags={"Apartment"},
     *    security={
     *      {"passport": {}},
     *    },
     *    summary="Get active all records",
     *    description="Get active all records",
     *    @OA\Response(
     *      response=204,
     *      description="Successful operation, returned all",
     *      @OA\MediaType(
     *        mediaType="application/json",
     *      )
     *    ),
     *  )
     */
    public function index()
    {
        $apartments = Apartment::getAll();
        return response()->json($apartments,200);
    }
    
    /**
     *  @OA\Post(
     *    path="/apartment",
     *    tags={"Apartment"},
     *    security={
     *      {"passport": {}},
     *    },
     *    summary="Store a new record",
     *    description="Store a new record",
     *    @OA\Parameter(
     *      name="name",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *        type="string"
     *      )
     *    ),
     *    @OA\Parameter(
     *      name="description",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *        type="string"
     *      )
     *    ),
     *    @OA\Parameter(
     *      name="floor_area_size",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *        type="numeric"
     *      )
     *    ),
     *    @OA\Parameter(
     *      name="price_per_month",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *        type="numeric"
     *      )
     *    ),
     *    @OA\Parameter(
     *      name="n_rooms",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *        type="integer"
     *      )
     *    ),
     *    @OA\Parameter(
     *      name="latitude",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *        type="float"
     *      )
     *    ),
     *    @OA\Parameter(
     *      name="longitude",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *        type="float"
     *      )
     *    ),
     *    @OA\Parameter(
     *      name="rented",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *        type="boolean"
     *      )
     *    ),
     *    @OA\Parameter(
     *      name="realtor_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *        type="integer"
     *      )
     *    ),
     *    @OA\Response(
     *      response=200,
     *      description="Successful operation",
     *      @OA\MediaType(
     *        mediaType="application/json",
     *      )
     *    ),
     *  )
     */
    public function store(StoreApartmentRequest $request)
    {
        //We need to check in case of realtors, if the realtor_id is he same as the current_id
        if(Auth::user()->role_id == User::$REALTOR_ROLE && $request->realtor_id != Auth::user()->id)
            return response()->json(['success' => false, 'message' => 'You are not allowed to save this record'], 403);
        //We check if realtor_id exists as an user
        if(!User::find($request->realtor_id))
            return response()->json(['success' => false, 'message' => 'Realtor not exist'], 403);
        $result = Apartment::storeRecord($request);
        if($result['success']){
            return response()->json(['success' => true, 'record' => $result['record']],200);
        }else{
            return response()->json(['success' => false, 'message' => $result['message']],422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    
    /**
     *  @OA\Put(
     *    path="/apartment/{id}",
     *    tags={"Apartment"},
     *    security={
     *      {"passport": {}},
     *    },
     *    summary="Update record",
     *    description="Update record",
     *    @OA\Parameter(
     *      name="apartment_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *        type="integer"
     *      )
     *    ),
     *    @OA\Parameter(
     *      name="name",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *        type="string"
     *      )
     *    ),
     *    @OA\Parameter(
     *      name="description",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *        type="string"
     *      )
     *    ),
     *    @OA\Parameter(
     *      name="floor_area_size",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *        type="numeric"
     *      )
     *    ),
     *    @OA\Parameter(
     *      name="price_per_month",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *        type="numeric"
     *      )
     *    ),
     *    @OA\Parameter(
     *      name="n_rooms",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *        type="integer"
     *      )
     *    ),
     *    @OA\Parameter(
     *      name="latitude",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *        type="float"
     *      )
     *    ),
     *    @OA\Parameter(
     *      name="longitude",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *        type="float"
     *      )
     *    ),
     *    @OA\Parameter(
     *      name="rented",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *        type="boolean"
     *      )
     *    ),
     *    @OA\Parameter(
     *      name="realtor_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *        type="integer"
     *      )
     *    ),
     *    @OA\Response(
     *      response=200,
     *      description="Successful operation",
     *      @OA\MediaType(
     *        mediaType="application/json",
     *      )
     *    ),
     *  )
     */
    public function update(StoreApartmentRequest $request, $id)
    {
        //We need to check in case of realtors, if the realtor_id is he same as the current_id
        if(Auth::user()->role_id == User::$REALTOR_ROLE && $request->realtor_id != Auth::user()->id)
            return response()->json(['success' => false, 'message' => "You are not allowed to save this record"], 403);
        //We check if realtor_id exists as an user
        if(!User::find($request->realtor_id))
            return response()->json(['success' => false, 'message' => "Realtor not exist"], 403);
        $result = Apartment::updateRecord($request, $id);
        if($result['success'])
            return response()->json(['success' => true, 'message' => $result['record']],200);
        else
            return response()->json(['success' => false, 'message' => $result['message']], 404);
    }

    /**
     *  @OA\Delete(
     *    path="/v1/apartment/{id}",
     *    tags={"Apartment"},
     *    security={
     *      {"passport": {}},
     *    },
     *    summary="Delete record",
     *    description="Delete record",
     *    @OA\Parameter(
     *      name="id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *        type="integer"
     *      )
     *    ),
     *    @OA\Response(
     *      response=200,
     *      description="Successful operation",
     *      @OA\MediaType(
     *        mediaType="application/json",
     *      )
     *    ),
     *  )
     */
    public function destroy($id)
    {
        $apartment = Apartment::find($id);
        if(!$apartment)
            return response()->json(['success' => false, 'message' => 'This record not exist'], 403);
        //We need to check in case of realtors, if the realtor_id is he same as the current_id
        if(Auth::user()->role_id == User::$REALTOR_ROLE && $apartment->realtor_id != Auth::user()->user_id)
            return response()->json(['success' => false, 'message' => 'You are not allowed to save this record'], 403);
        $apartment->delete();
        return response()->json(['success' => true, 'message' => 'Success'],200);
    }
}
