<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon as Carbon;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use App\Http\Requests\StoreUserClientRequest;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests\ForgotRequest;
use App\Http\Requests\RestartPasswordRequest;

use Illuminate\Support\Facades\Password;

class UserController extends Controller
{    
    /**
     *  @OA\Post(
     *    path="/restart_password",
     *    tags={"User"},
     *    security={
     *      {"passport": {}},
     *    },
     *    summary="Restart password",
     *    description="Restart password",
     *    @OA\Parameter(
     *      name="email",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *        type="string"
     *      )
     *    ),
     *    @OA\Response(
     *      response=204,
     *      description="Successful operation, returned all",
     *      @OA\MediaType(
     *        mediaType="application/json",
     *      )
     *    ),
     *  )
     */
    public function restart_password(RestartPasswordRequest $request)
    {        
        $status = Password::reset(
            $request->only('email', 'password', 'password_confirmation', 'token'),
            function ($user, $password) use ($request) {
                $user->forceFill([
                    'password' => Hash::make($password)
                ])->setRememberToken(Str::random(60));
    
                $user->save();

            }
        );
        if($status == Password::PASSWORD_RESET)
            return response()->json(['success' => true],200);
        else
            return response()->json(['success' => false, 'message' => 'This token is not assigned to any user'],422);
    }

    /**
     *  @OA\Post(
     *    path="/forgot",
     *    tags={"User"},
     *    security={
     *      {"passport": {}},
     *    },
     *    summary="Send email to restart password",
     *    description="Send email to restart password",
     *    @OA\Parameter(
     *      name="email",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *        type="string"
     *      )
     *    ),
     *    @OA\Response(
     *      response=204,
     *      description="Successful operation, returned all",
     *      @OA\MediaType(
     *        mediaType="application/json",
     *      )
     *    ),
     *  )
     */
    public function forgot(ForgotRequest $request)
    {        
        $status = Password::sendResetLink(
            $request->only('email')
        );
        if($status == Password::RESET_LINK_SENT){
            return response()->json(['success' => true, 'message' => $status],200);
        }else{
            return response()->json(['success' => false, 'message' => $status],422);
        }
    }

    /**
     *  @OA\Post(
     *    path="/register",
     *    tags={"User"},
     *    security={
     *      {"passport": {}},
     *    },
     *    summary="Register new user",
     *    description="Register new user",
     *    @OA\Parameter(
     *      name="name",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *        type="string"
     *      )
     *    ),
     *    @OA\Parameter(
     *      name="email",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *        type="string"
     *      )
     *    ),
     *    @OA\Parameter(
     *      name="password",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *        type="string"
     *      )
     *    ),
     *    @OA\Response(
     *      response=204,
     *      description="Successful operation, returned all",
     *      @OA\MediaType(
     *        mediaType="application/json",
     *      )
     *    ),
     *  )
     */
    public function register(StoreUserClientRequest $request)
    {
        $request->role_id = 3;
        $result = User::storeRecord($request);
        if($result['success']){
            return response()->json(['success' => true, 'record' => $result['record']],200);
        }else{
            return response()->json(['success' => false, 'message' => $result['message']],422);
        }
    }

    /**
     *  @OA\Get(
     *    path="/user",
     *    tags={"User"},
     *    security={
     *      {"passport": {}},
     *    },
     *    summary="Get active all records",
     *    description="Get active all records",
     *    @OA\Response(
     *      response=204,
     *      description="Successful operation, returned all",
     *      @OA\MediaType(
     *        mediaType="application/json",
     *      )
     *    ),
     *  )
     */
    public function index()
    {
        $users = User::getAll();
        return response()->json($users,200);
    }
    
    /**
     *  @OA\Post(
     *    path="/user",
     *    tags={"User"},
     *    security={
     *      {"passport": {}},
     *    },
     *    summary="Store a new record",
     *    description="Store a new record",
     *    @OA\Parameter(
     *      name="name",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *        type="string"
     *      )
     *    ),
     *    @OA\Parameter(
     *      name="email",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *        type="string"
     *      )
     *    ),
     *    @OA\Parameter(
     *      name="password",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *        type="string"
     *      )
     *    ),
     *    @OA\Parameter(
     *      name="role_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *        type="numeric"
     *      )
     *    ),
     *    @OA\Response(
     *      response=200,
     *      description="Successful operation",
     *      @OA\MediaType(
     *        mediaType="application/json",
     *      )
     *    ),
     *  )
     */
    public function store(StoreUserRequest $request)
    {   
        $result = User::storeRecord($request);
        if($result['success']){
          return response()->json(['success' => true, 'record' => $result['record']],200);
        }else{
          return response()->json(['success' => false, 'message' => $result['message']],422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    
    
    /**
     *  @OA\Put(
     *    path="/user",
     *    tags={"User"},
     *    security={
     *      {"passport": {}},
     *    },
     *    summary="Update record",
     *    description="Update record",
     *    @OA\Parameter(
     *      name="name",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *        type="string"
     *      )
     *    ),
     *    @OA\Parameter(
     *      name="email",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *        type="string"
     *      )
     *    ),
     *    @OA\Parameter(
     *      name="password",
     *      in="query",
     *      required=false,
     *      @OA\Schema(
     *        type="string"
     *      )
     *    ),
     *    @OA\Parameter(
     *      name="role_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *        type="numeric"
     *      )
     *    ),
     *    @OA\Response(
     *      response=200,
     *      description="Successful operation",
     *      @OA\MediaType(
     *        mediaType="application/json",
     *      )
     *    ),
     *  )
     */
    public function update(UpdateUserRequest $request, $id)
    {
        $result = User::updateRecord($request, $id);
        if($result)
            return response()->json(['success' => true, 'message' => $result['record']],200);
        else
            return response()->json(['success' => false, 'message' => $result['message']], 404);
    }

    /**
     *  @OA\Delete(
     *    path="/v1/user/{id}",
     *    tags={"User"},
     *    security={
     *      {"passport": {}},
     *    },
     *    summary="Delete record",
     *    description="Delete record",
     *    @OA\Parameter(
     *      name="id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *        type="integer"
     *      )
     *    ),
     *    @OA\Response(
     *      response=200,
     *      description="Successful operation",
     *      @OA\MediaType(
     *        mediaType="application/json",
     *      )
     *    ),
     *  )
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if(!$user)
            return response()->json(['success' => false, 'message' => 'This record not exist'], 403);
        //We need to check in case of realtors, if the realtor_id is he same as the current_id
        if(Auth::user()->role_id == User::$REALTOR_ROLE && $user->realtor_id != Auth::user()->user_id)
            return response()->json(['success' => false, 'message' => 'You are not allowed to save this record'], 403);
        if(Auth::id() == $id)
            return response()->json(['success' => false, 'message' => 'You cannot delete your own user'], 403);
        $user->delete();
        return response()->json(['success' => true, 'message' => 'Success'],200);
    }
}
