<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class SanctumController extends Controller
{    
    /**
     *  @OA\Post(
     *    path="/close",
     *    tags={"Close"},
     *    security={
     *      {"passport": {}},
     *    },
     *    summary="Close conection, and delete tokens",
     *    description="Close conection, and delete tokens",
     *    @OA\Response(
     *      response=204,
     *      description="Successful operation",
     *      @OA\MediaType(
     *        mediaType="application/json",
     *      )
     *    ),
     *  )
     */
    public function close(Request $request)
    {
        $user = User::find(Auth::id());
        $user->tokens()->delete();
        return true;
    }

    /**
     *  @OA\Post(
     *    path="/register",
     *    tags={"User"},
     *    security={
     *      {"passport": {}},
     *    },
     *    summary="Close conection, and delete tokens",
     *    description="Close conection, and delete tokens",
     *    @OA\Parameter(
     *      name="email",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *        type="string"
     *      )
     *    ),
     *    @OA\Parameter(
     *      name="password",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *        type="string"
     *      )
     *    ),
     *    @OA\Parameter(
     *      name="device_name",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *        type="string"
     *      )
     *    ),
     *    @OA\Response(
     *      response=204,
     *      description="Successful operation",
     *      @OA\MediaType(
     *        mediaType="application/json",
     *      )
     *    ),
     *  )
     */
    public function token(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
            'device_name' => 'required',
        ]);

        $user = User::where('email', $request->email)->first();

        if (! $user || ! Hash::check($request->password, $user->password)) {
            throw ValidationException::withMessages([
                'email' => ['The provided credentials are incorrect.'],
            ]);
        }
        
        switch($user->role_id){
            case User::$ADMIN_ROLE:
                $token = $user->createToken($request->device_name, ['admin']);
                break;
            case User::$REALTOR_ROLE:
                $token = $user->createToken($request->device_name, ['realtor']);
                break;
            case User::$CLIENT_ROLE:
                $token = $user->createToken($request->device_name, ['client']);
                break;
            default:
                $token = $user->createToken($request->device_name, ['client']);
                break;
        }

        $token = $token->toArray();
        $token['user'] = $user->toArray();
        
        return response()->json($token,200);
    }

}
