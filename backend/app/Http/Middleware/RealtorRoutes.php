<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RealtorRoutes
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(Auth::user()->tokenCan('realtor') || Auth::user()->tokenCan('admin'))
            return $next($request);
        else
            return response()->json(["success" => false, 'message' => "You not have permissions to access to this data"], 422);
    }
}
