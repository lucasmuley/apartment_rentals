import Vue from "vue";

const errorHandler = error => {
  const instance = new Vue({});
  error.config.errorHandler = false;

  if (typeof error.response.data.errors !== "undefined") {
    for (let field of Object.keys(error.response.data.errors)) {
      instance.$noty.error(error.response.data.errors[field][0]);
    }
    return error;
  }

  if (
    typeof error.response.data !== "undefined" &&
    error.response.data.message
  ) {
    instance.$noty.error(error.response.data.message);
    if (error.response.status === 401) {
      return error;
    }
  } else {
    if (typeof error.response.data !== "undefined") {
      for (let field of Object.keys(error.response.data)) {
        instance.$noty.error(error.response.data[field][0]);
      }
    } else {
      instance.$noty.error(error.toString());
      return error;
    }
  }
  return error;
};

export default errorHandler;
