import ApiService from "@/services/api.service";

const state = {
  token: "",
  role: "",
  user: false,
};

const getters = {
  token(state) {
    return state.token;
  },
  role(state) {
    return state.role;
  },
  user(state) {
    return state.user;
  },
};

const actions = {
  async login(context, data) {
    return await new Promise(resolve => {
      ApiService.post("sanctum/token", {
        email: data.email,
        password: data.password,
        device_name: "Vue",
      }).then(({ data }) => {
        context.commit("setToken", data);
        resolve(data);
      });
    });
  },
  logout(context) {
    return new Promise(resolve => {
      ApiService.setHeader();
      ApiService.post("api/sanctum/close").then(({ data }) => {
        context.commit("logout");
        resolve(data);
      });
    });
  },
  register(context, data) {
    return new Promise(resolve => {
      ApiService.post("api/register", data).then(({ data }) => {
        resolve(data);
      });
    });
  },
  forgot(context, data) {
    return new Promise(resolve => {
      ApiService.post("api/forgot", data).then(({ data }) => {
        resolve(data);
      });
    });
  },
  restart_password(context, data) {
    return new Promise(resolve => {
      ApiService.post("api/restart_password", data).then(({ data }) => {
        resolve(data);
      });
    });
  },
};

const mutations = {
  setToken(state, data) {
    state.token = data.plainTextToken;
    state.role = data.accessToken.abilities[0];
    state.user = data.user;
  },
  logout(state) {
    state.token = "";
    state.role = "";
    state.user = false;
  },
};

export default {
  state,
  actions,
  mutations,
  getters,
};
