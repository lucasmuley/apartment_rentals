import ApiService from "@/services/api.service";

const state = {
  users: [],
};

const getters = {
  users(state) {
    return state.users;
  },
};

const actions = {
  getUsers(context) {
    return new Promise(resolve => {
      ApiService.setHeader();
      ApiService.get("api/user").then(({ data }) => {
        context.commit("setUsers", data);
        resolve(data);
      });
    });
  },
  saveUser(context, data) {
    return new Promise(resolve => {
      let user = {
        ...data,
      };
      ApiService.setHeader();
      ApiService.post("api/user", user).then(({ data }) => {
        context.commit("addUser", data);
        resolve(data);
      });
    });
  },
  updateUser(context, user) {
    return new Promise(resolve => {
      ApiService.setHeader();
      ApiService.put("api/user/" + user.id, user).then(({ data }) => {
        context.commit("updateUser", data);
        resolve(data);
      });
    });
  },
  deleteUser(context, id) {
    return new Promise(resolve => {
      ApiService.setHeader();
      ApiService.delete("api/user/" + id).then(({ data }) => {
        context.commit("deleteUser", id);
        resolve(data);
      });
    });
  },
};

const mutations = {
  setUsers(state, data) {
    state.users = data;
  },
  addUser(state, data) {
    state.users.push(data.record);
  },
  updateUser(state, user) {
    state.users.forEach((us, index) => {
      if (us.id === user.id) {
        state.user.splice(index, 1, user);
      }
    });
  },
  deleteUser(state, id) {
    state.users.forEach((user, index) => {
      if (user.id === id) {
        state.users.splice(index, 1);
      }
    });
  },
};

export default {
  state,
  actions,
  mutations,
  getters,
};
