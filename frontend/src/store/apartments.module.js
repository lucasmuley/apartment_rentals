import ApiService from "@/services/api.service";

const state = {
  apartments: [],
};

const getters = {
  apartments(state) {
    return state.apartments;
  },
};

const actions = {
  getApartments(context) {
    return new Promise(resolve => {
      ApiService.setHeader();
      ApiService.get("api/apartment").then(({ data }) => {
        context.commit("setApartments", data);
        resolve(data);
      });
    });
  },
  searchApartments(context, data) {
    return new Promise(resolve => {
      ApiService.setHeader();
      ApiService.post("api/apartment/search", data).then(({ data }) => {
        resolve(data);
      });
    });
  },
  limitsApartments() {
    return new Promise(resolve => {
      ApiService.setHeader();
      ApiService.post("api/apartment/limits").then(({ data }) => {
        resolve(data);
      });
    });
  },
  saveApartment(context, apartment) {
    return new Promise(resolve => {
      ApiService.setHeader();
      ApiService.post("api/apartment", apartment).then(({ data }) => {
        context.commit("addApartment", data);
        resolve(data);
      });
    });
  },
  updateApartment(context, apartment) {
    return new Promise(resolve => {
      ApiService.setHeader();
      ApiService.put("api/apartment/" + apartment.id, apartment).then(
        ({ data }) => {
          context.commit("updateApartment", data);
          resolve(data);
        }
      );
    });
  },
  deleteApartment(context, id) {
    return new Promise(resolve => {
      ApiService.setHeader();
      ApiService.delete("api/apartment/" + id).then(({ data }) => {
        context.commit("deleteApartment", id);
        resolve(data);
      });
    });
  },
};

const mutations = {
  setApartments(state, data) {
    state.apartments = data;
  },
  addApartment(state, data) {
    state.apartments.push(data.record);
  },
  deleteApartment(state, id) {
    state.apartments.forEach((apartment, index) => {
      if (apartment.id === id) {
        state.apartments.splice(index, 1);
      }
    });
  },
  updateApartment(state, apartment) {
    state.apartments.forEach((apart, index) => {
      if (apart.id === apartment.id) {
        state.apartments.splice(index, 1, apartment);
      }
    });
  },
};

export default {
  state,
  actions,
  mutations,
  getters,
};
