import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";

import auth from "./auth.module";
import apartments from "./apartments.module";
import users from "./users.module";

Vue.use(Vuex);

const dataPersisted = createPersistedState({
  paths: ["auth", "apartments"],
});

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    auth,
    apartments,
    users,
  },
  plugins: [dataPersisted],
});
