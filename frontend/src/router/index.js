import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Register from "../views/Register.vue";
import Forgot from "../views/Forgot.vue";
import ResetPassword from "../views/ResetPassword.vue";
import UsersIndex from "../views/pages/users/Index.vue";
import UsersEdit from "../views/pages/users/Edit.vue";
import UsersNew from "../views/pages/users/New.vue";
import ApartmentsIndex from "../views/pages/apartments/Index.vue";
import ApartmentsEdit from "../views/pages/apartments/Edit.vue";
import ApartmentsNew from "../views/pages/apartments/New.vue";
import Search from "../views/pages/search/Index.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/register",
    name: "Register",
    component: Register,
  },
  {
    path: "/forgot",
    name: "Forgot",
    component: Forgot,
  },
  {
    path: "/reset-password/:token",
    name: "ResetPassword",
    component: ResetPassword,
  },
  {
    path: "/users",
    name: "Users",
    component: UsersIndex,
  },
  {
    path: "/users/new",
    name: "Users New",
    component: UsersNew,
  },
  {
    path: "/users/:id",
    name: "Users Edit",
    component: UsersEdit,
  },
  {
    path: "/apartments",
    name: "Apartments",
    component: ApartmentsIndex,
  },
  {
    path: "/apartments/new",
    name: "Apartments New",
    component: ApartmentsNew,
  },
  {
    path: "/apartments/:id",
    name: "Apartments Edit",
    component: ApartmentsEdit,
  },
  {
    path: "/search",
    name: "Search",
    component: Search,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
