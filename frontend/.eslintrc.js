module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    "plugin:vue/essential",
    "plugin:prettier/recommended",
    "@vue/prettier",
  ],
  rules: {
    "no-console": "error",
    "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",
    "max-len": ["error", { code: 360 }],
    indent: [
      "error",
      2,
      {
        ignoredNodes: ["TemplateLiteral"],
        SwitchCase: 1,
      },
    ],
    quotes: ["error", "double"],
    eqeqeq: ["error", "always"],
  },
  parserOptions: {
    ecmaVersion: 6,
    parser: "babel-eslint",
  },
};
